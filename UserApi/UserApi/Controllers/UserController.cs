﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UserApi.Models;
using UserApi.Models.Requests;
using UserApi.Models.View;
using UserApi.Processors;

namespace UserApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : BaseController
    {
        private readonly IUserProcessor _userProcessor;

        public UserController(IUserProcessor userProcessor) => _userProcessor = userProcessor;

        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(UserView), (int)HttpStatusCode.OK)]
        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateRequest request)
        {
            return Ok(await _userProcessor.CreateUserAsync(request.User));
        }

        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ProblemDetails), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(UserView), (int)HttpStatusCode.OK)]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(GetByIdRequest request)
        {


            UserView result = await _userProcessor.GetUserAsync(request?.Id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
