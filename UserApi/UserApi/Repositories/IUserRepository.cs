﻿using MongoDB.Driver;
using System.Threading.Tasks;
using UserApi.Models.Domain;


namespace UserApi.Repositories
{
    public interface IUserRepository
    {
        IMongoCollection<UserDomain> Collection { get; set; }

        public void CreateUser(UserDomain user);
        public Task<UserDomain> GetUser(string userId);

    }
}
