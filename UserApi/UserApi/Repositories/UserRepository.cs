﻿using MongoDB.Driver;
using System.Threading.Tasks;
using UserApi.Models.Domain;


namespace UserApi.Repositories
{
    public class UserRepository : IUserRepository
    {
        public UserRepository(IUserDatabaseSettings settings)
        {
            MongoClient client = new MongoClient(settings.ConnectionString);
            IMongoDatabase database = client.GetDatabase(settings.DatabaseName);
            Collection = database.GetCollection<UserDomain>(settings.UsersCollectionName);

        }

        public IMongoCollection<UserDomain> Collection { get; set; }


        public void CreateUser(UserDomain user)
        {
            Collection.InsertOneAsync(user);
        }

        public async Task<UserDomain> GetUser(string userId)
        {

            var userDomain = await Collection.Aggregate().Match(Builders<UserDomain>.Filter.Eq(user => user.Id, userId)).FirstOrDefaultAsync();
            //return userDomain== null ? null : userDomain;
            return userDomain;



        }

    }
}
