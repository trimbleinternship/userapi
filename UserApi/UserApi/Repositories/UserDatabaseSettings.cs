﻿

namespace UserApi.Repositories
{
    public class UserDatabaseSettings : IUserDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string UsersCollectionName { get; set; }

    }
}
