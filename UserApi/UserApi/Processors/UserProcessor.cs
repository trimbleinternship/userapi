﻿using AutoMapper;
using System;
using System.Threading.Tasks;
using UserApi.Models.Domain;
using UserApi.Models.View;
using UserApi.Repositories;

namespace UserApi.Processors
{
    public class UserProcessor : IUserProcessor
    {
        private readonly IUserRepository _repository;
        private readonly IMapper _mapper;


        public UserProcessor(IMapper mapper, IUserRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<UserView> CreateUserAsync(UserView user, bool triggerUserCreatedEvent = true)
        {
            UserDomain userDomain = _mapper.Map<UserDomain>(user);
            if (userDomain == null)
            {
                return null;
            }
            if (string.IsNullOrWhiteSpace(userDomain.Id))
            {
                userDomain.Id = Guid.NewGuid().ToString();
            }
            _repository.CreateUser(userDomain);
            return user;
        }

        public async Task<UserView> GetUserAsync(string userId)
        {

            var result = await _repository.GetUser(userId);
            return result == null ? null : _mapper.Map<UserView>(result);
        }


    }
}
