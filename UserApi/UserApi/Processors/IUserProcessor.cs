﻿
using System.Threading.Tasks;
using UserApi.Models.View;

namespace UserApi.Processors
{
    public interface IUserProcessor
    {
        Task<UserView> CreateUserAsync(UserView user, bool triggerUserCreatedEvent = true);

        Task<UserView> GetUserAsync(string userId);
    }
}
