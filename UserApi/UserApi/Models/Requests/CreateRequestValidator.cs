﻿using FluentValidation;


namespace UserApi.Models.Requests
{
    public class CreateRequestValidator : AbstractValidator<CreateRequest>
    {
        public CreateRequestValidator()
        {
            RuleFor(x => x.User.Firstname).NotNull().NotEmpty();
            RuleFor(x => x.User.Surname).NotNull().NotEmpty();
        }
    }
}
