﻿using Microsoft.AspNetCore.Mvc;

namespace UserApi.Models.Requests
{
    //[SwaggerExclude]
    public class GetByIdRequest
    {
        [FromQuery]
        public string Id { get; set; }
    }
}
