﻿using Microsoft.AspNetCore.Mvc;
using UserApi.Models.View;

namespace UserApi.Models.Requests
{
    public class CreateRequest
    {
        [FromBody]
        public UserView User { get; set; }
    }
}
