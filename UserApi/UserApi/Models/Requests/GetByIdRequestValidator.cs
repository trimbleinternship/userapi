﻿using FluentValidation;


namespace UserApi.Models.Requests
{

    public class GetByIdRequestValidator : AbstractValidator<GetByIdRequest>
    {
        public GetByIdRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotNull()
                .NotEmpty();

        }
    }

}
