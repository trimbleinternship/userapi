﻿

namespace UserApi.Models.View
{
    public class UserView
    {
        public string Id { get; set; }
        public string Firstname { get; set; }

        public string Surname { get; set; }
        public string Age { get; set; }
    }
}
