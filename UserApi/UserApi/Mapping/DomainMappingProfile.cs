﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserApi.Models.Domain;
using UserApi.Models.View;

namespace UserApi.Mapping
{
    public class DomainMappingProfile : Profile
    {
        public DomainMappingProfile()
        {
            CreateMap<UserDomain, UserView>();
            CreateMap<UserView, UserDomain>();
        }
    }
}
