﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Shouldly;
using System.Net;
using System.Threading.Tasks;
using UserApi.Controllers;
using UserApi.Models.Requests;
using UserApi.Models.View;
using UserApi.Processors;
using Xunit;

namespace UserApi.UnitTest.Controllers
{
    public class UserControllerTests
    {
        private readonly UserController _sut;
        private readonly Mock<IUserProcessor> _processor;

        public UserControllerTests()
        {
            _processor = new Mock<IUserProcessor>(MockBehavior.Strict);
            _sut = new UserController(_processor.Object);
        }


        [Fact]
        public async void CreateUser_Returns_OkObjectResult()
        {
            UserView expected = new UserView();
            _processor.Setup(m => m.CreateUserAsync(expected, true)).ReturnsAsync(expected);
            CreateRequest request = new CreateRequest { User = expected };

            IActionResult actionResult = await _sut.CreateUser(request);

            OkObjectResult okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            okObjectResult.StatusCode.ShouldBe((int?)HttpStatusCode.OK);
            okObjectResult.Value.ShouldBeOfType<UserView>();
            var actual = okObjectResult.Value as UserView;
            actual.ShouldNotBe(null);
            expected.ShouldBe(actual);
            _processor.Verify(x => x.CreateUserAsync(expected,true), Times.Once());

        }


        [Fact]
        public async Task GetUser_Returns_OkObjectResult()
        {
            UserView expected = new UserView { Id = "123", Surname = "surname", Firstname = "firstname", Age = "10" };
            _processor.Setup(m => m.GetUserAsync(It.IsAny<string>())).Returns(Task.FromResult(expected));
            GetByIdRequest request = new GetByIdRequest { Id = "123" };

            //act
            IActionResult actionResult = await _sut.GetUser(request);

            //assert
            OkObjectResult okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            okObjectResult.StatusCode.ShouldBe((int?)HttpStatusCode.OK);
            okObjectResult.Value.ShouldBeOfType<UserView>();
            var actual = okObjectResult.Value as UserView;
            actual.ShouldNotBe(null);
            expected.Firstname.ShouldBe(actual.Firstname);
            expected.Surname.ShouldBe(actual.Surname);
            expected.Id.ShouldBe(actual.Id);
            expected.Age.ShouldBe(actual.Age);
            _processor.Verify(x => x.GetUserAsync(It.IsAny<string>()), Times.Once());
        }

        [Fact]
        public async Task GetUser_Returns_NotFound()
        {

            _processor.Setup(m => m.GetUserAsync(It.IsAny<string>())).Returns(Task.FromResult<UserView>(null));
            GetByIdRequest request = new GetByIdRequest { Id = "123" };

            //act
            IActionResult actionResult = await _sut.GetUser(request);

            //assert
            NotFoundResult okObjectResult = Assert.IsType<NotFoundResult>(actionResult);
            Assert.Equal((int?)HttpStatusCode.NotFound, okObjectResult.StatusCode);
            _processor.Verify(x => x.GetUserAsync(It.IsAny<string>()), Times.Once());
        }

    }
}
