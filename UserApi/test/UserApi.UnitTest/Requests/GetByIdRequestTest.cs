﻿
using FluentValidation.Results;
using UserApi.Models.Requests;
using Xunit;

namespace UserApi.UnitTest.Requests
{
    public class GetByIdRequestTest
    {
        [Fact]
        public void Should_have_errors_when_user_id_is_null()
        {
            GetByIdRequest request = new GetByIdRequest { Id = null };
            GetByIdRequestValidator validator = new GetByIdRequestValidator();
            ValidationResult result = validator.Validate(request);

            Assert.False(result.IsValid);
        }
    }
}
