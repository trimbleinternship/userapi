﻿using FluentValidation.Results;
using UserApi.Models.Requests;
using UserApi.Models.View;
using Xunit;


namespace UserApi.UnitTest.Requests
{

    public class CreateRequestTest
    {
        [Fact]
        public void Should_have_errors_when_user_name_is_null()
        {
            CreateRequest request = new CreateRequest { User = new UserView() };
            CreateRequestValidator validator = new CreateRequestValidator();
            ValidationResult result = validator.Validate(request);

            Assert.False(result.IsValid);
        }
    }
}
