﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserApi.Models.Domain;
using UserApi.Models.View;
using UserApi.Processors;
using UserApi.Repositories;
using Xunit;

namespace UserApi.UnitTest.Processors
{
    public class UserProcessorTests
    {
        private readonly Mock<IUserRepository> _repository;
        private readonly UserProcessor _sut;
        private readonly Mock<IMapper> _mapper;

        public UserProcessorTests()
        {
            _mapper = new Mock<IMapper>(MockBehavior.Strict);
            _repository = new Mock<IUserRepository>();
            _sut = new UserProcessor(_mapper.Object, _repository.Object);
        }

        [Fact]
        public void CreateUserAsync_Returns_user()
        {
            UserView expected = new UserView
            {
                Id = "test_id",
                Firstname = "testuser",
                Surname = "testuser",
                Age = "20",
            };
            UserDomain userDomain = new UserDomain();
            _mapper.Setup(x => x.Map<UserDomain>(expected)).Returns(userDomain);
            _repository.Setup(x => x.CreateUser(userDomain));

            var result = (UserView)_sut.CreateUserAsync(expected, true).Result;

            _repository.Verify(x => x.CreateUser(userDomain), Times.Once());
            result.Id.ShouldBe(expected.Id);

        }
        [Fact]
        public void GetUserAsync_Returns_user()
        {

            string userId = "string";
            _repository.Setup(x => x.GetUser(userId));

            var result = _sut.GetUserAsync(userId).Result;
            _repository.Verify(x => x.GetUser(userId), Times.Once());
            

        }
    }
}
