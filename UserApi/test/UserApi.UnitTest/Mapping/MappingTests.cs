﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserApi.Mapping;
using UserApi.Models.Domain;
using UserApi.Models.View;
using Xunit;

namespace UserApi.UnitTest.Mapping
{
    public class MappingTests
    {
        private readonly IMapper _mapper;

        public MappingTests()
        {
            var mapperConfig = new MapperConfiguration(x => x.AddProfile<DomainMappingProfile>());
            _mapper = mapperConfig.CreateMapper();
        }

        [Fact]
        public void TestEstimateMapping()
        {
            UserView userView = new UserView
            {
                Id = "test_id",
                Firstname = "testuser",
                Surname = "testuser",
                Age = "20",
            };

            UserDomain domainLayer = _mapper.Map<UserDomain>(userView);


            Assert.Equal("test_id", userView.Id);
            Assert.Equal("testuser", userView.Firstname);
            Assert.Equal("testuser", userView.Surname);
            Assert.Equal("20", userView.Age);
            userView = _mapper.Map<UserView>(domainLayer);

            Assert.Equal("test_id", userView.Id);
            Assert.Equal("testuser", userView.Firstname);
            Assert.Equal("testuser", userView.Surname);
            Assert.Equal("20", userView.Age);
        }
    }
}
